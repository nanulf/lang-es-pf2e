Hooks.once('init', () => {

    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'lang-es-pf2e',
            lang: 'es',
            dir: 'compendium'
        });
    }
});
